# IDC-Deep-Learning-Project

In this project I have made a website which can classify various the images uploaded by the user. This project uses the “MobileNet” model from the ml5 project to train the image classifier. The model is trained on data from ImageNet, which is an image database organized according to the WordNet hierarchy (currently only the nouns), in which each node of the hierarchy is depicted by hundreds and thousands of images. The project has been instrumental in advancing computer vision and deep learning research. The data is available for free to researchers for non-commercial use.

Link to website : https://saket-18.github.io/IDC-Deep-Learning-Project/
